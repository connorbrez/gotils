package gotils

import (
    "reflect"
    "testing"
)

func TestMap(t *testing.T) {
    input := []int{1, 2, 3, 4}
    expected := []int{2, 4, 6, 8}
    actual := Map(input, func(v int) int {
        return v * 2
    })
    if !reflect.DeepEqual(actual, expected) {
        t.Errorf("Expected %v, got %v", expected, actual)
    }
}
