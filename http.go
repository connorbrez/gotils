package gotils

import (
    "encoding/json"
    "errors"
    "fmt"
    "io"
    "log/slog"
    "net"
    "net/http"
    "net/url"
    "strconv"
    "strings"
    "time"

    "github.com/gorilla/handlers"
)

// Error stores error information with an HTTP status code and a message.
type Error struct {
	StatusCode int    `json:"statusCode"`
	Message    string `json:"message"`
}

//JSONLogLine Stores information for constructing JSON logs
type JSONLogLine struct {
	URL        string `json:"url"`
	Time       string `json:"time"`
	StatusCode string `json:"status"`
	Size       string `json:"size"`
	Host       string `json:"host"`
	Method     string `json:"method"`
	Proto      string `json:"proto"`
	User       string `json:"user"`
	Message    string `json:"message"`
}

// GetBearerToken Extracts the Authorization header from an HTTP request
// Validates the format (must be "Bearer token")
// Returns the token if valid, or an error if not
func GetBearerToken(req *http.Request) (string, error) {
	authorizationHeader, err := GetHeader(req, "Authorization")
	if err != nil {
		return "", err
	}

	authHeaderSplit := strings.Split(authorizationHeader, " ")
	if len(authHeaderSplit) != 2 {
		return "", errors.New("invalid authentication header received, header requires both a token type and token")
	}

	tokenType := authHeaderSplit[0]
	if !strings.EqualFold(tokenType, "bearer") {
		return "", errors.New("invalid authentication header received, invalid token type")
	}

	return authHeaderSplit[1], nil
}

//GetHeader retrieves a specific header from the request.
// Returns an error if the header is missing.
func GetHeader(req *http.Request, key string) (string, error) {
	h := req.Header.Get(key)
	if h == "" {
		return "", fmt.Errorf("missing %s", key)
	}

	return h, nil
}


// WriteError writes an error message as a JSON response.
// Uses WriteJSON to handle the actual writing process.
func WriteError(m string, s int, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	b := Error{
		StatusCode: s,
		Message:    m,
	}

	WriteJSON(b, s, w)
}
// WriteJSON converts data to JSON format and writes it to the HTTP response.
// Sets the appropriate content type, status code, and handles any JSON marshaling errors.
func WriteJSON(v interface{}, s int, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")

	b, err := json.Marshal(v)
	if err != nil {
		slog.Error("Failed to marshal json", "error", err)
		w.WriteHeader(http.StatusInternalServerError)
		_, err = w.Write([]byte(err.Error()))
		if err != nil {
			slog.Error("Failed to write to response writer...", "error", err)
		}
		return
	}
	w.WriteHeader(s)
	_, err = w.Write(b)
	if err != nil {
		slog.Error("Failed to write to response writer...", "error", err)
	}
}



// ReadBody reads the body of an HTTP request.
// Unmarshalls the JSON data into the given interface
func ReadBody(r *http.Request, v interface{}) error {
	defer func(Body io.ReadCloser) {
        err := Body.Close()
        if err != nil {
			slog.Error("failed to call Body.Close", "error", err)
        }
    }(r.Body)

	bytes, err := io.ReadAll(r.Body)
	if err != nil {
		return fmt.Errorf("error reading request body: %w", err)
	}

	err = json.Unmarshal(bytes, &v)
	if err != nil {
		return fmt.Errorf("error unmarshalling request body: %w", err)
	}

	return nil
}



//JSONLoggingWriter constructs a JSONLogLine and writes it to the provided io.Writer
func JSONLoggingWriter(writer io.Writer, params handlers.LogFormatterParams) {
	buf, err := buildJSONLine(params.Request, params.URL, params.TimeStamp, params.StatusCode, params.Size)
	if err != nil {
		slog.Error("Error building json log...", "error", err)
		_, err = writer.Write([]byte("Error building json log..."))
		if err != nil {
			slog.Error("Failed to write to response writer...", "error", err)
		}
		return
	}

	buf = append(buf, '\n')
	_, err = writer.Write(buf)
	if err != nil {
		slog.Error("Failed to write to response writer...", "error", err)
	}
}

func buildJSONMessage(m string) (string, error) {
	l := struct{Message string}{Message: m}
	logLine, err := json.Marshal(l)
	if err != nil {
		return "", err
	}
	return string(logLine), nil

}

func buildJSONLine(req *http.Request, url url.URL, ts time.Time, status int, size int) ([]byte, error) {
	username := "-"
	if url.User != nil {
		if name := url.User.Username(); name != "" {
			username = name
		}
	}

	host, _, err := net.SplitHostPort(req.RemoteAddr)

	if err != nil {
		host = req.RemoteAddr
	}

	uri := req.RequestURI

	// Requests using the CONNECT method over HTTP/2.0 must use
	// the authority field (aka r.Host) to identify the target.
	// Refer: https://httpwg.github.io/specs/rfc7540.html#CONNECT
	if req.ProtoMajor == 2 && req.Method == "CONNECT" {
		uri = req.Host
	}
	if uri == "" {
		uri = url.RequestURI()
	}

	logLine := JSONLogLine{URL: uri, Time: ts.Format("02/Jan/2006:15:04:05 -0700"), StatusCode: strconv.Itoa(status), Size: strconv.Itoa(size), Host: host, Method: req.Method, Proto: req.Proto, User: username}
	logLine.Message = fmt.Sprintf("%v %v %v", logLine.Time, logLine.StatusCode, logLine.URL)

	buf, err := json.Marshal(logLine)

	if err != nil {
		s, err := buildJSONMessage(fmt.Sprintf("failed to marshal log line: %s", err))
		if err != nil {
			return []byte(""), err
		}
		return []byte(s), nil
	}

	return buf, nil
}


