module gitlab.com/connorbrez/gotils

go 1.22

require github.com/gorilla/handlers v1.5.2

require github.com/felixge/httpsnoop v1.0.4 // indirect
