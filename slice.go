package gotils

// Map transforms a slice of elements of type T into a slice of elements of type U by applying the function f to each element of the input slice
func Map[T, U any](ts []T, f func(T) U) []U {
	us := make([]U, len(ts))
	for i := range ts {
		us[i] = f(ts[i])
	}
	return us
}

