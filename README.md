# Gotils [![Go Reference](https://pkg.go.dev/badge/gitlab.com/connorbrez/gotils.svg)](https://pkg.go.dev/gitlab.com/connorbrez/gotils) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/connorbrez/gotils)](https://goreportcard.com/report/gitlab.com/connorbrez/gotils) [![coverage report](https://gitlab.com/connorbrez/gotils/badges/master/coverage.svg)](https://gitlab.com/connorbrez/gotils/-/commits/master)


A collection of commonly used go functions 