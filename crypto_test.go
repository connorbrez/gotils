package gotils

import (
    "os"
    "testing"
)


const validTestPrivateKey = `-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEAwuA+DAWJM0elk/LurcQc7cy7QiOeJ4RW08o0D5NqDgU9sZN5
DMgjXy7cSdA0DkvTJWWRq02CRTiIopTWrZIer9sdFVmWbFYuVEPJTft2Qz7VyHob
7okuA8KrBvOPiWJGFFzzIGpm5u/1s4GRwNpY1wfX27sQIAPJFUjsrmKYhKn5BUny
rQVKnio6yLVGUaRnnrfFOA39k95LUzziWHIyooj/XkUxvM06uYgm9/MX3+DEXSX6
JuZVSYGW4Cla2dmOR7ABa5eUI8gH2UcQUrb7frXQ6yntIZDldz9QQBqy0op9vHTx
UqvxEUAANlFkAFyhONo/001yUtQxj3JNnrqBKkVz1DJIh8z8gz3PTjJqUYCTVtDK
ju2VYgk32lUIJiUugVmFLnX/poHIQ0T7F7HoR+nULPu8YnH6+L+0RWbi/heyUNd0
AzjWdiCKAg+lruzkuh1gx3/jOU+IOZ5biP2s35lkqpgLEDTfxAuc1hSAC0tVP8uW
FxH8FqHq6hQVlzUQYP6nM+h5J+LRnIFtuas6GiW4+We569K5uy26HtC98vyNCh2s
4/ZCCsiCvvpxssjzYfE4XvQTWPNvXZOe23UmLgfu2NJ6zZfhWX5vmYrCdB9TZGBN
ULj446yI6616uh9pF53pzdoR+mizXilPYYVtGXhjNkLAg/1zpjAwCpKKGsUCAwEA
AQKCAgACkO9gOyZuXXApvZY1BZMX8o04tr1badu22D/NQLSx0JHaZ8W9TFli5HwU
IMC+u3LhWDAN4Gxpuu/ZrDoTeQwcZrdAU7iuXognpejdtQT+p7Iq/KingHjxpbuY
0NZACeJAXGT7NUt7mQdUQM2Ji5aF5l2KUDFLKqGYPSiYt0Wo4ZAc6LdgXeR4pre1
cuWlJ5QRu/vF3DFNpCy7wN5+KarPBxzNwf2ArfcrAEb7FuFmmlAbpedgYcIXwLQa
6ZB/s+SyvveV0h4qRnUMAGqPnEc9iOliX1/3BDBxXI0S+wcFzLsRIz0WSJtZDc2y
6Wl2s8GPvRgAEVIDEzY/3FJ3V59hyDDnMrmvMt1EgSJkyT0bEFNHJssbSba6d/UY
2g+8S6TAhdXhd/RkXSiz0IxqPO047bADwVKxic98eP+iXYfgbeEelV8qh701zbjD
k623kfvoz2ZtjWOwgLpvn6Nd8ID/1xBq3L1Umh6jkfAklp5Bfqwc/rqeUFK1kqBW
jX1O+rwiUp57HauL7ncyQrn+WPm3qrg06pUCRIowPFRfZl/qbbvATB93Qqwm/1wK
1AHH5ttqn4E4xt31zjC4VvE80GcrvrSNx0ZanVBZjyJxzX1f0I7gjj9aK6dUKX88
UPVpH9sFAIoQFkdFGjZea8rVPzYdCkIG3NOellkF1ZZ0L8YVwQKCAQEAxV88Bb3P
kJLjnctzlrpl15auL0esWT2dXU4rOaD7EQnQ+wJe70KuB+3MJvmoCtSZgfhVxLTt
qqdwaV2IWKGj3wNNFos61k+HjEFDtQ9V/uE+f1Wz8zogq1zdumD7zypdm/QeQG5w
mku2kfwxep2Yh954VkHpEE213LxsjgxytdzLy1wDq94ymGnJEQpXE77ijd72fAqz
v0/lTKPxeEPJvbB0QQNo97RZPPvZO8YWyPPJCQ6OMd4GjTcXoi+Do05WTo2S6aM+
KFoFwvGnJb49rPkspG4wP1T1pe76TTAADXok1DXcmVZJWI5J1OKlpYv/IQ4fBXwd
W+3iKM63rk3HtQKCAQEA/MMzIU3uW7fTtuLzVpotFvBa4+HJHypUCqtyP7cntASa
Plqsklk0h8aZKAXy9nCSjFy/YjSWJj5jHQ5AHxeJwZGCkdFgltAKe8qVSUuFN7wN
xNUaITSMgOMr0BtOoQfkwI1YFks+LCgq98jUblMjSRFtU6BnRn6mYYmBbliAvSpe
sSCNCkbUqBsT1SuYSu3TfvisXVHSdlsR8YSbeU+A27sOVcLgs2GXleXENb0jYAPn
iiyyxYyhjrQcg4HOlQlWbQ4SDGZG24QaCmWkGeoEoZwJt773PZIvooQMBB40zMYN
6biAuagAH24pV+i3tVpamg50bXn4Sm0XNBZXRgvQ0QKCAQEAp6gMHnxr8s0z2HhY
SbMwi3+Nms/xUSULpZSfFybt3j1ObcpHxkoH9uhCwODVo82QUF0Yzi06rfnpfTdc
eJ1lzCzLsNi4NA/+fl96GjVawVWodxXf9D5uPeJ23rk/bWz5c15czDKEKC0kesGV
biisxs1tvyfhyF2anFTaiZd9YYqa+rXA0J/ArsM9buIvsbA2VnkNO7UDlwFRMFca
6SP8HMvlFMzGv5NNMoZks2fDNxQOs0jP9QA65H3l73kZAwGqQrP0Fc77H9/IgR4w
89i0H1jSWSLYjROoTzm/GQ4rHUeiPTk7mG05hKXphZK5TvSq1fNe2pPhhJN/x1ZB
pSig+QKCAQEAwN7SLR+iOipTSsJynCqZLV/KewjWxwkth9a7HxNLIHpCmEHoXOPk
yZYpV/jyJEpSCPjcsLsft57zUkCFfTQg0auOzmXOJkYpObyjnYIa7SR5/S/++Ed4
zwCJdgrgPNAhJSy1go13q4oa/2gncOfPjPnXSnCjXY9WtiaOtPyjf7TM/Kux852I
t52xzH+vfb1sHP7zx2GgDt//GSPu3upjkUdy8M37V+WoHaFtzvO8Ao/4Jd9lqohc
3t9gwphSYjb109FgxEVm3Co5FKXqSjvfvNGsOKFnV3wtDP1ljd7eu0KFa0p2LzSa
o2ET+xiA1clUSSmufbsMUv5bLku93J03gQKCAQAcTF6OnI73C1pwYyPTD6Q62bpI
Zo3c9IBHoqmn0qfPIccs9u5SqmYVny0FFiYR6uIgOFd5niJYFis9Ju8/SvizlATA
njnwHvVydPLnp8S9/Bt5KqDrx0FcmTe8cpkY85/RAS+uSN2fAhn0unLLInuth1V3
Aj1Rm1U7vYRl1druYizszJ/pBB5ztEcJDekXO4qNQp26OlWO6UkE6uNWBm0fN2h6
FVOUNGRh7mCUayN5j8CauEkchTrYj/0/3t75L+gS5XkJHmL3W4qCXE3A0apEp0A5
SbnhR2Seke/O983fWHRl/4L16mzVAQcQ48sMyKpdfMYgB96pQdgwhiBsAy+I
-----END RSA PRIVATE KEY-----`

const invalidTestPrivateKey = `-----BEGIN RSA PRIVATE KEY-----
MIIJKQIBAAKCAgEAwuA+DAWJM0elk/LurcQc7cy7QiOeJ4RW08o0D5NqDgU9sZN5
DMgjXy7cSdA0DkvTJWWRq02CRTiIopTWrZIer9sdFVmWbFYuVEPJTft2Qz7VyHob
7okuA8KrBvOPiWJGFFzzIGpm5u/1s4GRwNpY1wfX27sQIAPJFUjsrmKYhKn5BUny
rQVKnio6yLVGUaRnnrfFOA39k95LUzziWHIyooj/XkUxvM06uYgm9/MX3+DEXSX6
JuZVSYGW4Cla2dmOR7ABa5eUI8gH2UcQUrb7frXQ6yntIZDldz9QQBqy0op9vHTx
UqvxEUAANlFkAFyhONo/001yUtQxj3JNnrqBKkVz1DJIh8z8gz3PTjJqUYCTVtDK
ju2VYgk32lUIJiUugVmFLnX/poHIQ0T7F7HoR+nULPu8YnH6+L+0RWbi/heyUNd0
AzjWdiCKAg+lruzkuh1gx3/jOU+IOZ5biP2s35lkqpgLEDTfxAuc1hSAC0tVP8uW
FxH8FqHq6hQVlzUQYP6nM+h5J+LRnIFtuas6GiW4+We569K5uy26HtC98vyNCh2s
4/ZCCsiCvvpxssjzYfE4XvQTWPNvXZOe23UmLgfu2NJ6zZfhWX5vmYrCdB9TZGBN
ULj446yI6616uhawdkjajiohbdauwhilPYYVtGXhjNkLAg/1zpjAwCpKKGsUCAwEA
AQKCAgACkO9gOyZuXXApvZY1BZMX8o04tr1badu22D/NQLSx0JHaZ8W9TFli5HwU
IMC+u3LhWDAN4Gxpuu/ZrDoTeQwcZrdAU7iuXognpejdtQT+p7Iq/KingHjxpbuY
0NZACeJAXGT7NUt7mQdUQM2Ji5aF5l2KUDFLKqGYPSiYt0Wo4ZAc6LdgXeR4pre1
cuWlJ5QRu/vF3DFNpCy7wN5+KarPBxzNwf2ArfcrAEb7FuFmmlAbpedgYcIXwLQa
6ZB/s+SyvveV0h4qRnUMAGqPnEc9iOliX1/3BDBxXI0S+wcFzLsRIz0WSJtZDc2y
6Wl2s8GPvRgAEVIDEzY/3FJ3V59hyDDnMrmvMt1EgSJkyT0bEFNHJssbSba6d/UY
2g+8S6TAhdXhd/RkXSiz0IxqPO047bADwVKxic98eP+iXYfgbeEelV8qh701zbjD
k623kfvoz2ZtjWOwgLpvn6Nd8ID/1xBq3L1Umh6jkfAklp5Bfqwc/rqeUFK1kqBW
jX1O+rwiUp57HauL7ncyQrn+WPm3qrg06pUCRIowPFRfZl/qbbvATB93Qqwm/1wK
1AHH5ttqn4E4xt31zjC4VvE80GcrvrSNx0ZanVBZjyJxzX1f0I7gjj9aK6dUKX88
UPVpH9sFAIoQFkdFGjZea8rVPzYdCkIG3NOellkF1ZZ0L8YVwQKCAQEAxV88Bb3P
kJLjnctzlrpl15auL0esWT2dXU4rOaD7EQnQ+wJe70KuB+3MJvmoCtSZgfhVxLTt
qqdwaV2IWKGj3wNNFos61k+HjEFDtQ9V/uE+f1Wz8zogq1zdumD7zypdm/QeQG5w
mku2kfwxep2Yh954VkHpEE213LxsjgxytdzLy1wDq94ymGnJEQpXE77ijd72fAqz
v0/lTKPxeEPJvbB0QQNo97RZPPvZO8YWyPPJCQ6OMd4GjTcXoi+Do05WTo2S6aM+
KFoFwvGnJb49rPkspG4wP1T1pe76TTAADXok1DXcmVZJWI5J1OKlpYv/IQ4fBXwd
W+3iKM63rk3HtQKCAQEA/MMzIU3uW7fTtuLzVpotFvBa4+HJHypUCqtyP7cntASa
Plqsklk0h8aZKAXy9nCSjFy/YjSWJj5jHQ5AHxeJwZGCkdFgltAKe8qVSUuFN7wN
xNUaITSMgOMr0BtOoQfkwI1YFks+LCgq98jUblMjSRFtU6BnRn6mYYmBbliAvSpe
sSCNCkbUqBsT1SuYSu3TfvisXVHSdlsR8YSbeU+A27sOVcLgs2GXleXENb0jYAPn
iiyyxYyhjrQcg4HOlQlWbQ4SDGZG24QaCmWkGeoEoZwJt773PZIvooQMBB40zMYN
6biAuagAH24pV+i3tVpamg50bXn4Sm0XNBZXRgvQ0QKCAQEAp6gMHnxr8s0z2HhY
SbMwi3+Nms/xUSULpZSfFybt3j1ObcpHxkoH9uhCwODVo82QUF0Yzi06rfnpfTdc
eJ1lzCzLsNi4NA/+fl96GjVawVWodxXf9D5uPeJ23rk/bWz5c15czDKEKC0kesGV
biisxs1tvyfhyF2anFTaiZd9YYqa+rXA0J/ArsM9buIvsbA2VnkNO7UDlwFRMFca
6SP8HMvlFMzGv5NNMoZks2fDNxQOs0jP9QA65H3l73kZAwGqQrP0Fc77H9/IgR4w
89i0H1jSWSLYjROoTzm/GQ4rHUeiPTk7mG05hKXphZK5TvSq1fNe2pPhhJN/x1ZB
pSig+QKCAQEAwN7SLR+iOipTSsJynCqZLV/KewjWxwkth9a7HxNLIHpCmEHoXOPk
yZYpV/jyJEpSCPjcsLsft57zUkCFfTQg0auOzmXOJkYpObyjnYIa7SR5/S/++Ed4
zwCJdgrgPNAhJSy1go13q4oa/2gncOfPjPnXSnCjXY9WtiaOtPyjf7TM/Kux852I
t52xzH+vfb1sHP7zx2GgDt//GSPu3upjkUdy8M37V+WoHaFtzvO8Ao/4Jd9lqohc
3t9gwphSYjb109FgxEVm3Co5FKXqSjvfvNGsOKFnV3wtDP1ljd7eu0KFa0p2LzSa
o2ET+xiA1clUSSmufbsMUv5bLku93J03gQKCAQAcTF6OnI73C1pwYyPTD6Q62bpI
Zo3c9IBHoqmn0qfPIccs9u5SqmYVny0FFiYR6uIgOFd5niJYFis9Ju8/SvizlATA
njnwHvVydPLnp8S9/Bt5KqDrx0FcmTe8cpkY85/RAS+uSN2fAhn0unLLInuth1V3
Aj1Rm1U7vYRl1druYizszJ/pBB5ztEcJDekXO4qNQp26OlWO6UkE6uNWBm0fN2h6
FVOUNGRh7mCUayN5j8CauEkchTrYj/0/3t75L+gS5XkJHmL3W4qCXE3A0apEp0A5
SbnhR2Seke/O983fWHRl/4L16mzVAQcQ48sMyKpdfMYgB96pQdgwhiBsAy+I
-----END RSA PRIVATE KEY-----`


func TestLoadRSAPrivateKeyFromBytes(t *testing.T) {
	// Test Valid Key
	privateKey, err := LoadRSAPrivateKeyFromBytes([]byte(validTestPrivateKey))
	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if privateKey == nil {
		t.Fatalf("Expected a valid private key, got nil")
	}
	
	// Test Invalid Key
	privateKey, err = LoadRSAPrivateKeyFromBytes([]byte(invalidTestPrivateKey))
	if err == nil {
		t.Fatalf("Expected error, got %v", err)
	}

	if privateKey != nil {
		t.Fatalf("Expected a invalid private key")
	}
}

func TestLoadRSAPrivateKeyFromPath(t *testing.T) {
	// Create a temporary file to simulate the private key file
	tmpfile, err := os.CreateTemp("", "testkey")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tmpfile.Name())

	// Write a sample RSA private key to the temporary file
	if _, err := tmpfile.Write([]byte(validTestPrivateKey)); err != nil {
		t.Fatal(err)
	}

	// Make sure to flush writes to the file
	if err := tmpfile.Sync(); err != nil {
		t.Fatal(err)
	}

	// Test the function with the path of the temporary file
	privateKey, err := LoadRSAPrivateKeyFromPath(tmpfile.Name())
	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if privateKey == nil {
		t.Fatalf("Expected a valid private key, got nil")
	}
}