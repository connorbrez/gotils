package gotils

import "testing"

func TestRandomString(t *testing.T) {
	expected := 5
	actual := len(RandomString(expected))
	
	if actual != expected {
		t.Errorf("Expected %v, got %v", expected, actual)

	}
}