package gotils

import (
    "bytes"
    "encoding/json"
    "fmt"
    "github.com/gorilla/handlers"
    "net/http"
    "net/http/httptest"
    "net/url"
    "strings"
    "testing"
    "time"
)



func TestGetAuthHeader(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	expectedToken := "some-token"
	req.Header.Set("Authorization", fmt.Sprintf("bearer %v", expectedToken))
	
	
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		actualToken, err := GetBearerToken(req)
		if err != nil {
			t.Errorf(err.Error())
		}
		if actualToken != expectedToken {
			t.Errorf("Expected %s, but got %s", expectedToken, actualToken)
		}
	})

	handler.ServeHTTP(rr, req)
}

func TestGetHeader(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Set("X-Custom-Header", "custom-value")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		headerValue, err := GetHeader(r, "X-Custom-Header")
		if err != nil {
			t.Errorf(err.Error())
		}
		if headerValue != "custom-value" {
			t.Errorf("Expected header value 'custom-value', got '%s'", headerValue)
		}
	})

	handler.ServeHTTP(rr, req)
}

func TestReadBody(t *testing.T) {
	req, err := http.NewRequest("GET", "/", strings.NewReader("{\"message\":\"hello world\"}"))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var body struct{Message string `json:"message"`}
		err := ReadBody(r, &body)
		if err != nil {
			t.Errorf(err.Error())
		}
		if &body == nil {
			t.Errorf("Expected body to have value, is nil")
		}
		
		if body.Message != "hello world" {
			t.Errorf("Expected body.Message to have value \"hello world\", is %v", body.Message)
		}
	})

	handler.ServeHTTP(rr, req)
}

func TestWriteJSON(t *testing.T) {
	rr := httptest.NewRecorder()
   	var data struct{Message string `json:"message"`}
	data.Message = "hello world"

    WriteJSON(data, http.StatusOK, rr)

    if status := rr.Code; status != http.StatusOK {
        t.Errorf("expected status code %v, got %v", http.StatusOK, status)
    }

    expectedResponse := `{"message":"hello world"}`
    if rr.Body.String() != expectedResponse {
        t.Errorf("expected response body %v, got %v", expectedResponse, rr.Body.String())
    }

    if contentType := rr.Header().Get("Content-Type"); contentType != "application/json" {
        t.Errorf("expected content type %v, got %v", "application/json", contentType)
    }
}

func TestWriteError(t *testing.T) {
	rr := httptest.NewRecorder()
    WriteError("error", http.StatusBadGateway, rr)

    if status := rr.Code; status != http.StatusBadGateway {
        t.Errorf("expected status code %v, got %v", http.StatusBadGateway, status)
    }

    expectedResponse := `{"statusCode":502,"message":"error"}`
    if rr.Body.String() != expectedResponse {
        t.Errorf("expected response body %v, got %v", expectedResponse, rr.Body.String())
    }

    if contentType := rr.Header().Get("Content-Type"); contentType != "application/json" {
        t.Errorf("expected content type %v, got %v", "application/json", contentType)
    }
}

func TestJSONLoggingWriter(t *testing.T) {
	buffer := &bytes.Buffer{}

	req, err := http.NewRequest("GET", "https://example.com", nil)
	if err != nil {
		t.Fatalf("Failed to create request: %v", err)
	}
	req.RemoteAddr = "123.123.123.123:45678"

	params := handlers.LogFormatterParams{
		Request:    req,
		URL:        url.URL{Path: "/test"},
		TimeStamp:  time.Unix(1615852800, 0).UTC(),
		StatusCode: 200,
		Size:       1234,
	}

	JSONLoggingWriter(buffer, params)

	expected := `{"url":"/test","time":"16/Mar/2021:00:00:00 +0000","status":"200","size":"1234","host":"123.123.123.123","method":"GET","proto":"HTTP/1.1","user":"-","message":"16/Mar/2021:00:00:00 +0000 200 /test"}`
	actual := buffer.String()

	if expected+"\n" != actual {
		t.Fatalf("Expected %s but got %s", expected, actual)
	}
}

func TestBuildJSONMessage(t *testing.T) {
	expected := `{"Message":"test message"}`
	result, err := buildJSONMessage("test message")
	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}
	if result != expected {
		t.Fatalf("Expected %s, got %s", expected, result)
	}
}

func TestBuildJSONLine(t *testing.T) {
	req := httptest.NewRequest("GET", "https://example.com/foo", nil)
	req.RemoteAddr = "127.0.0.1:12345"
	testURL := url.URL{Scheme: "https", Host: "example.com", Path: "/foo"}
	timestamp := time.Date(2023, 10, 10, 10, 10, 10, 0, time.UTC)
	status := 200
	size := 123

	expected := JSONLogLine{
		URL:        "https://example.com/foo",
		Time:       "10/Oct/2023:10:10:10 +0000",
		StatusCode: "200",
		Size:       "123",
		Host:       "127.0.0.1",
		Method:     "GET",
		Proto:      "HTTP/1.1",
		User:       "-",
		Message:    "10/Oct/2023:10:10:10 +0000 200 https://example.com/foo",
	}

	expectedBuf, err := json.Marshal(expected)
	if err != nil {
		t.Fatalf("Failed to marshal expected JSON: %v", err)
	}

	buf, err := buildJSONLine(req, testURL, timestamp, status, size)
	if err != nil {
		t.Fatalf("buildJSONLine() error: %v", err)
	}
	if !bytes.Equal(buf, expectedBuf) {
		t.Errorf("Expected %s, got %s", expectedBuf, buf)
	}
}
