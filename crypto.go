package gotils

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"os"
)


// LoadRSAPrivateKeyFromPath - This function reads an RSA private key from a file specified by the path.
// It first reads the file into keyBytes.
// In case of an error reading the file, it returns nil along with the error.
// If successful, it calls LoadRSAPrivateKeyFromBytes to decode the key.
func LoadRSAPrivateKeyFromPath(path string) (*rsa.PrivateKey, error) {
	keyBytes, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return LoadRSAPrivateKeyFromBytes(keyBytes)
}

// LoadRSAPrivateKeyFromBytes decodes a RSA private key from a byte slice b.
// It uses pem.Decode to decode the byte slice into a keyPEM block.
// The function checks if the keyPEM.Type is "RSA PRIVATE KEY". If not, it returns an error.
// It then parses the PEM block into an *rsa.PrivateKey using x509.ParsePKCS1PrivateKey.
// If parsing is successful, it returns the RSA private key. Otherwise, it returns an error.
func LoadRSAPrivateKeyFromBytes(b []byte) (*rsa.PrivateKey, error) {
	keyPEM, _ := pem.Decode(b)

	if keyPEM == nil {
		return nil, fmt.Errorf("invalid private key")
	}

	if keyPEM.Type != "RSA PRIVATE KEY" {
		return nil, fmt.Errorf("private key is of the wrong type, %v. Expected RSA", keyPEM.Type)
	}

	parseResult, err := x509.ParsePKCS1PrivateKey(keyPEM.Bytes)
	if err != nil {
		return nil, err
	}
	return parseResult, nil
}
